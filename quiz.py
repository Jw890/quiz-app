import tkinter as tk
from tkinter import font  as tkfont
from tkinter import messagebox
from tkinter import simpledialog
import csv
import pandas as pd

class QuizApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.shared_data = {
            "quizName": tk.StringVar(),
            "qNum": 1,
            "questName": tk.StringVar(),
            "numOfQ": tk.StringVar(),
            "qName": tk.StringVar()
            }

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        self.container = tk.Frame(self)
        self.container.pack(side="top", fill="both", expand=True)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)

        self.pageList = [startPage, createMultiQuiz, newQuestion, multiQuestion, openQuestion, quizSelect, quizDisplay]
        
        self.frames = {}
        for F in (self.pageList):
            page_name = F.__name__
            frame = F(parent=self.container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_new_frame("startPage")

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()

    def show_new_frame(self, page_name):

        page = page_name
        container = self.container
        
        self.frames = {}
        for F in (self.pageList):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.tryThis(page)
        
    def tryThis(self, page):
        frame = self.frames[page]
        frame.tkraise()

class startPage(tk.Frame):

    def __init__(self, parent, controller):
        
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text = "Welcome", font=controller.title_font)
        label.grid(row = 0, pady=10, padx = 10)

        button1 = tk.Button(self, text = 'Start Quiz', command = lambda: controller.show_new_frame("quizSelect"))
        button1.grid(row=2, column = 1)

        button2 = tk.Button(self, text = 'Create New Quiz', command = lambda: controller.show_new_frame("createMultiQuiz"))
        button2.grid(row=2, column = 2)
        
class createMultiQuiz(tk.Frame):

    def __init__(self, parent, controller):
        
        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        label = tk.Label(self, text = 'Create a new multi choice quiz', font=controller.title_font)
        label.grid(row=0)

        createLabel = tk.Label(self, text = 'Please enter a title for your multi choice quiz')
        self.entryTitle = tk.Entry(self)
        createLabel.grid(row = 1, column = 0)
        self.entryTitle.grid(row = 1, column = 1)
        
        button1 = tk.Button(self, text = 'Submit', command = self.getInfo)
        button2 = tk.Button(self, text = 'Back Home', command = lambda: controller.show_new_frame("startPage"))
        button1.grid(row = 50, column = 0)
        button2.grid(row = 50, column = 1)

    def getInfo(self):
        quizName = self.entryTitle.get()
        if not quizName:
            messagebox.showinfo("Quiz App", "You have not entered a Quiz Name!")
        else:
            choice = messagebox.askquestion("Quiz App", "You have named your Quiz : " + quizName + "\n Do you wish to continue", icon='warning')

        if choice == "yes":
            self.controller.shared_data["quizName"] = quizName
            self.saveInfo()
            self.controller.show_new_frame('newQuestion')
        else:
            choice = messagebox.askquestion("Quiz App", "You haven't given your quiz a name!", icon='warning')

    def saveInfo(self):
        
        quizName = self.entryTitle.get()
        with open(quizName+ ".csv", "a") as csv_file:
            csv_app = csv.writer(csv_file)

class newQuestion(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        quizName = self.controller.shared_data["quizName"]
        self.qType = tk.IntVar()
        
        label = tk.Label(self, text = 'Quiz: ' + str(quizName), font=controller.title_font)
        label.grid(row = 0, column = 0)

        qNum = self.controller.shared_data["qNum"]
        
        label1 = tk.Label(self, text='Please enter your question for question #' + str(qNum))
        label1.grid(row = 1, column = 0)
        self.entryQ = tk.Entry(self)
        self.entryQ.grid(row = 1, column = 1)

        spaceLabel = tk.Label(self)
        spaceLabel.grid(row = 3)

        qLabel = tk.Label(self, text="What kind of question?")
        qLabel.grid(row = 4, column = 0) 
        multiQ = tk.Radiobutton(self, text="Multi choice question", padx = 20, variable=self.qType, value = 0)
        multiQ.grid(row = 5, column = 0, sticky = "w")
        self.qNum = tk.StringVar()
        self.multiDrop = tk.OptionMenu(self, self.qNum, '2', '3', '4','5','6', command = self.getDrop)
        self.multiDrop.grid(row = 6, column = 1)
        openQ = tk.Radiobutton(self, text="A question that requires an answer", padx = 20, variable=self.qType, value = 1)
        openQ.grid(row = 7, column = 0, sticky = "w")

        button = tk.Button(self, text = 'Submit', command = self.getInfo)
        button.grid(row = 50, column = 0)
                
    def getDrop(self, value):
        self.qNum = value

    def getInfo(self):
        qName = self.entryQ.get()

        if not qName:
            messagebox.showinfo('Quiz App', 'You have not entered a question!', icon = 'warning')

        else:
            self.controller.shared_data["qName"] = qName
            qType = self.qType.get()
            
            if qType == 0:
                self.controller.shared_data["numOfQ"] = self.qNum
                self.controller.show_new_frame('multiQuestion')
                
      
            if qType == 1:
                self.controller.show_new_frame('openQuestion')

class multiQuestion(tk.Frame):

    def __init__(self, parent, controller):
        
        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        label = tk.Label(self, text="Multichoice Question", font=controller.title_font)
        label.grid(row = 0)
        #add question number and quiz name
        label = tk.Label(self, text='Question: ' + str(self.controller.shared_data["qName"]))
        label.grid(row = 1)

        self.qNum = self.controller.shared_data["numOfQ"]
        numbers = '0123456789'
        if str(self.qNum) not in numbers:
            pass
        else:

            if int(self.qNum) == 2:
                label1 = tk.Label(self, text='Answer 1')
                label1.grid(row = 2, column = 0)
                label2 = tk.Label(self, text='Answer 2')
                label2.grid(row = 3, column = 0)
                self.entry1 = tk.Entry(self)
                self.entry1.grid(row = 2, column = 1)
                self.entry2 = tk.Entry(self)
                self.entry2.grid(row = 3, column = 1)
            if int(self.qNum) == 3:
                label1 = tk.Label(self, text='Answer 1')
                label1.grid(row = 2, column = 0)
                label2 = tk.Label(self, text='Answer 2')
                label2.grid(row = 3, column = 0)
                label3 = tk.Label(self, text='Answer 3')
                label3.grid(row = 4, column = 0)
                self.entry1 = tk.Entry(self)
                self.entry1.grid(row = 2, column = 1)
                self.entry2 = tk.Entry(self)
                self.entry2.grid(row = 3, column = 1)
                self.entry3 = tk.Entry(self)
                self.entry3.grid(row = 4, column = 1)
            if int(self.qNum) == 4:
                label1 = tk.Label(self, text='Answer 1')
                label1.grid(row = 2, column = 0)
                label2 = tk.Label(self, text='Answer 2')
                label2.grid(row = 3, column = 0)
                label3 = tk.Label(self, text='Answer 3')
                label3.grid(row = 4, column = 0)
                label4 = tk.Label(self, text='Answer 4')
                label4.grid(row = 5, column = 0)
                self.entry1 = tk.Entry(self)
                self.entry1.grid(row = 2, column = 1)
                self.entry2 = tk.Entry(self)
                self.entry2.grid(row = 3, column = 1)
                self.entry3 = tk.Entry(self)
                self.entry3.grid(row = 4, column = 1)
                self.entry4 = tk.Entry(self)
                self.entry4.grid(row = 5, column = 1)
            if int(self.qNum) == 5:
                label1 = tk.Label(self, text='Answer 1')
                label1.grid(row = 2, column = 0)
                label2 = tk.Label(self, text='Answer 2')
                label2.grid(row = 3, column = 0)
                label3 = tk.Label(self, text='Answer 3')
                label3.grid(row = 4, column = 0)
                label4 = tk.Label(self, text='Answer 4')
                label4.grid(row = 5, column = 0)
                label5 = tk.Label(self, text='Answer 5')
                label5.grid(row = 6, column = 0)
                self.entry1 = tk.Entry(self)
                self.entry1.grid(row = 2, column = 1)
                self.entry2 = tk.Entry(self)
                self.entry2.grid(row = 3, column = 1)
                self.entry3 = tk.Entry(self)
                self.entry3.grid(row = 4, column = 1)
                self.entry4 = tk.Entry(self)
                self.entry4.grid(row = 5, column = 1)
                self.entry5 = tk.Entry(self)
                self.entry5.grid(row = 6, column = 1)
            if int(self.qNum) == 6:
                label1 = tk.Label(self, text='Answer 1')
                label1.grid(row = 2, column = 0)
                label2 = tk.Label(self, text='Answer 2')
                label2.grid(row = 3, column = 0)
                label3 = tk.Label(self, text='Answer 3')
                label3.grid(row = 4, column = 0)
                label4 = tk.Label(self, text='Answer 4')
                label4.grid(row = 5, column = 0)
                label5 = tk.Label(self, text='Answer 5')
                label5.grid(row = 6, column = 0)
                label6 = tk.Label(self, text='Answer 6')
                label6.grid(row = 7, column = 0)
                self.entry1 = tk.Entry(self)
                self.entry1.grid(row = 2, column = 1)
                self.entry2 = tk.Entry(self)
                self.entry2.grid(row = 3, column = 1)
                self.entry3 = tk.Entry(self)
                self.entry3.grid(row = 4, column = 1)
                self.entry4 = tk.Entry(self)
                self.entry4.grid(row = 5, column = 1)
                self.entry5 = tk.Entry(self)
                self.entry5.grid(row = 6, column = 1)
                self.entry6 = tk.Entry(self)
                self.entry6.grid(row = 7, column = 1)
                      

        button = tk.Button(self, text = 'submit', command = self.getInfo)
        button.grid(row = 50, column = 2)

    def getInfo(self):
        
        counter = 1
        qName = self.controller.shared_data["qName"]
        
        self.aList = [qName]

        while counter < int(self.qNum):
            
            if counter > int(self.qNum):
                break
            else:
                a1 = self.entry1.get()
                if not a1:
                    messagebox.showinfo('Quiz App', 'You have not entered an answer for Answer 1!', icon = 'warning')
                    break
                else:
                    self.aList.append(a1)
            counter += 1
            
            if counter > int(self.qNum):
                break
            else:
                a2 = self.entry2.get()
                if not a2:
                    messagebox.showinfo('Quiz App', 'You have not entered an answer for Answer 2!', icon = 'warning')
                    break
                else:
                    self.aList.append(a2)
            counter += 1
                    
            if counter > int(self.qNum):
                break
            else:
                a3 = self.entry3.get()
                if not a3:
                    messagebox.showinfo('Quiz App', 'You have not entered an answer for Answer 3!', icon = 'warning')
                    break
                else:
                    self.aList.append(a3)
            counter += 1
            
            if counter > int(self.qNum):
                break
            else:
                a4 = self.entry4.get()
                if not a4:
                    messagebox.showinfo('Quiz App', 'You have not entered an answer for Answer 4!', icon = 'warning')
                    break
                else:
                    self.aList.append(a4)
            counter += 1
            
            if counter > int(self.qNum):
                break
            else:
                a5 = self.entry5.get()
                if not a5:
                    messagebox.showinfo('Quiz App', 'You have not entered an answer for Answer 5!', icon = 'warning')
                    break
                else:
                    self.aList.append(a5)
            counter += 1
                    
            if counter > int(self.qNum):
                break
            else:
                a6 = self.entry6.get()
                if not a6:
                    messagebox.showinfo('Quiz App', 'You have not entered an answer for Answer 6!', icon = 'warning')
                    break
                else:
                    self.aList.append(a6)
            counter += 1
            
        self.saveInfo()

    def saveInfo(self):
            
        quizName = self.controller.shared_data["quizName"]
        
        with open(quizName+ ".csv", mode='w') as csv_file:
            csv_file = csv.writer(csv_file)
            csv_file.writerow(['question', 'question1', 'question2', 'question3', 'question4', 'question5', 'question6'])
            csv_file.writerow(self.aList)
                           
class openQuestion(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="This does not work", font=controller.title_font)
        label.grid(row = 0)
        
class quizSelect(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Select Quiz", font=controller.title_font)
        label.grid(row = 0)

        quizs = []
        
        with open('quizs.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                quizs.append(row['Quiz'])
        quizNum = len(quizs)

        counter = 0

        while counter < quizNum:
            
            if counter >= int(quizNum):
                break
            else:
                button1 = tk.Button(self, text=quizs[counter], command = lambda: self.startQuiz(button1['text']))
                button1.grid(row = 1)
                counter += 1
                
            if counter >= int(quizNum):
                break
            else:
                button2 = tk.Button(self, text=quizs[counter], command = lambda: self.startQuiz(button2['text']))
                button2.grid(row = 2)
                counter += 1
                
            if counter >= int(quizNum):
                break
            else:
                button3 = tk.Button(self, text=quizs[counter], command = lambda: self.startQuiz(button3['text']))
                button3.grid(row = 3)
                counter += 1
                
            if counter >= int(quizNum):
                break
            else:
                button4 = tk.Button(self, text=quizs[counter], command = lambda: self.startQuiz(button4['text']))
                button4.grid(row = 4)
                counter += 1

            if counter >= int(quizNum):
                break
            else:
                button5 = tk.Button(self, text=quizs[counter], command = lambda: self.startQuiz(button5['text']))
                button5.grid(row = 5)
                counter += 1

            if counter >= int(quizNum):
                break
            else:
                #textName = quizs[counter]
                buttonNxtPage = tk.Button(self, text='Next Page')
                buttonNxtPage.grid(row = 6, column = 2)
                
    def startQuiz(self, qName):
        print(qName)
        goog = pd.read_csv(str(qName) + '.csv')
        print(goog)

class quizDisplay(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Quiz Page", font=controller.title_font)
        label.grid(row = 0)

app = QuizApp()
app.mainloop()
