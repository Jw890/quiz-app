import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import csv


LARGE_FONT = ("Verdana", 20)

class SeaofBTCApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        
        tk.Tk.__init__ (self, *args, **kwargs)
        container = tk.Frame(self)

        tk.Tk.wm_title(self, "Quiz App")

        container.pack(side = "top", fill = "both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        self.shared_data = {
            "quizName": tk.StringVar(),
            "test": "hello"
        }
        
        for F in(startPage, createMultiQuiz, newQuestion, multiQuestion, openQuestion):

            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
            
        self.show_frame(startPage)

    def show_frame(self, cont):

        frame = self.frames[cont]

        frame.tkraise()

class startPage(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller=controller

        label = tk.Label(self, text="Hey brooo", font=LARGE_FONT)
        label.grid(row = 0, pady = 10, padx = 10)

        button1 = ttk.Button(self, text = "Create new quiz", command = lambda: controller.show_frame(createMultiQuiz))
        button1.grid(row = 1) 

class createMultiQuiz(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="Create a new multi choice quiz", font=LARGE_FONT)
        label.grid(row = 0)
        
        self.entryTitle = ttk.Entry(self)
        createLabel = ttk.Label(self, text= "Please enter the title for your multi choice quiz")
        createLabel.grid(row = 1, column = 0)
        self.entryTitle.grid(row = 1, column = 1)

        button1 = ttk.Button(self, text = "Submit", command = self.getInfo)
        button1.grid(row = 50, column = 0)
        button2 = ttk.Button(self, text = "Back to home", command = lambda: controller.show_frame(startPage))
        button2.grid(row = 50, column = 1)
        

    def getInfo(self):
        quizName = self.entryTitle.get()
        self.controller.shared_data["quizName"] = quizName
        print(self.controller.shared_data["quizName"] + " hello")
        if not quizName:
            messagebox.showinfo("Quiz App", "You have not entered a Quiz Name!")
        else:
            choice = messagebox.askquestion("Quiz App", "You have named your Quiz : " + quizName + "\n Do you wish to continue", icon='warning')

        if choice == "yes":
            self.saveInfo()
            self.controller.show_frame(newQuestion)
        else:
            choice = messagebox.askquestion("Quiz App", "You haven't given your quiz a name!", icon='warning')

    def saveInfo(self):
        quizName = self.entryTitle.get()
        with open(quizName+ ".csv", "a") as csv_file:
            csv_app = csv.writer(csv_file)

class newQuestion(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.qType = tk.IntVar()

        self.quizName = self.controller.shared_data["quizName"]
        self.dictTest = self.controller.shared_data["test"]
        
        label = tk.Label(self, text="Create a new Question for Quiz: " + str(self.quizName) + str(self.dictTest), font=LARGE_FONT)
        label.grid(row = 0)

        createLabel = ttk.Label(self, text= "Please enter your question for question number #")
        self.entryTitle = ttk.Entry(self)
        createLabel.grid(row = 1, column = 0)
        self.entryTitle.grid(row = 1, column = 1)

        spaceLabel =tk.Label(self)
        spaceLabel.grid(row = 2)

        qLabel = tk.Label(self, text="What kind of question?")
        qLabel.grid(row = 3, column = 0) 
        multiQ = tk.Radiobutton(self, text="Multi choice question", padx = 20, variable=self.qType, value = 0)
        multiQ.grid(row = 4, column = 0, sticky = "w")
        openQ = tk.Radiobutton(self, text="A question that requires an answer", padx = 20, variable=self.qType, value = 1)
        openQ.grid(row = 5, column = 0, sticky = "w")

        button1 = ttk.Button(self, text = "Submit", command = self.getInfo)
        button1.grid(row = 50, column = 0)
        button2 = ttk.Button(self, text = "Back to home", command = lambda: controller.show_frame(startPage))
        button2.grid(row = 50, column = 1)

    @property
    def test(self):
        return self.quizName

    def getInfo(self):
        qName = self.entryTitle.get()
        if not qName:
            choice = messagebox.askquestion("Quiz App", "You have not entered a question!", icon='warning')
        else:
            print(qName) # need to add this to a variable and save it
        
            qType = self.qType.get()
            if qType == 0:
                self.controller.show_frame(multiQuestion)
            if qType == 1:
                self.controller.show_frame(openQuestion)
        
class multiQuestion(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="New mulitchoice question for ", font=LARGE_FONT)
        label.grid(row = 0)
        
        spaceLabel =tk.Label(self)
        spaceLabel.grid(row = 1)
        
        qLabel = tk.Label(self, text="Quiz name here")
        qLabel.grid(row = 2)
        
        button2 = ttk.Button(self, text = "Back to home", command = lambda: controller.show_frame(startPage))
        button2.grid(row = 50, column = 1)

    def saveInfo(self):
        with open("test" + ".csv", "a") as csv_file:
            csv_app = csv.writer(csv_file)
            csv_app.writerow(a)
            
class openQuestion(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="New question that requires an answer", font=LARGE_FONT)
        label.grid(row = 0)

        button2 = ttk.Button(self, text = "Back to home", command = lambda: controller.show_frame(startPage))
        button2.grid(row = 50, column = 1)

app = SeaofBTCApp()
app.mainloop()
