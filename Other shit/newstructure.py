import tkinter as tk
from tkinter import ttk

class main(tk.Tk):

    def __init__(self, *args, **kwargs):
        
        tk.Tk.__init__ (self, *args, **kwargs)
        container = tk.Frame(self)

        tk.Tk.wm_title(self, "Quiz App")

        container.pack(side = "top", fill = "both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.Test = startPage(container, self)
        self.Test1 = test1(container, self)
        self.Test2 = test2(container, self)

        self.frames = [startPage, test1, test2]

        self.pages = 0
        
        self.show_frame(self.Test)

        

    def show_frame(self, cont):

        frame = self.Test
        print(self.Test)
        self.frames[0] = frame
        frame.grid(row=0, column=0, sticky="nsew")

        frame.tkraise()
        
    def show_new_frame(self, cont):

        frame = self.Test1
        print(self.Test1)
        self.frames[0] = frame
        frame.grid(row=0, column=0, sticky="nsew")

        frame.tkraise()

    def show_new_frame2(self, cont):

        hello = 'Hello'
        
        frame = self.Test2
        print(self.Test2)
        self.frames[0] = frame
        frame.grid(row=0, column=0, sticky="nsew")

        frame.tkraise()
        
class startPage(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller=controller
        
        label = tk.Label(self, text="Hey brooo")
        label.grid(row = 0, pady = 10, padx = 10)

        button1 = ttk.Button(self, text = "test1", command = lambda: controller.show_new_frame(test1))
        button1.grid(row = 1) 
        
        print("test - start")

class test1(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller=controller
        
        label = tk.Label(self, text="Test1")
        label.grid(row = 0, pady = 10, padx = 10)
        
        button1 = ttk.Button(self, text = "test2", command = lambda: controller.show_new_frame2(test2))
        button1.grid(row = 1)
        
        print("test - test1")

class test2(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller=controller
        
        label = tk.Label(self, text="Test2")
        label.grid(row = 0, pady = 10, padx = 10)

        button1 = ttk.Button(self, text = "test2", command = lambda: controller.show_new_frame2(hello))
        button1.grid(row = 1)
              
        print("test - test2")

    def test(self):
        test123 = self.hello
        print(test123)

app = main()
app.mainloop()
